Dueligheds undervisning
=======================

#### Dueligheds undervisning ved "Loa sailing team"

###### Eksamensopgave:
- [Eksamensopgave](https://gitlab.com/jepnel/duelighed/raw/master/Ballen-Odden%20havn%20C.pdf?inline=false)

###### Datoer:
Lørdag d. 3/11-2018 Opgaver side 113 og 117  
Lørdag d. 1/12-2018  
Lørdag d. 2/2-2019  
Søndag d. 17/2-2019  

Ekasmen: Lørdag d. 9/3-2009 kl. 0900

- [Slides til søvejsregler](https://gitlab.com/jepnel/duelighed/raw/master/soevejsregler.pdf?inline=false)
- [Slides til vagthold](https://gitlab.com/jepnel/duelighed/raw/master/Vagt.pdf?inline=false)
- [Slides til navigation](https://gitlab.com/jepnel/duelighed/raw/master/Navigation.pdf?inline=false)
- [Slides til søsikkerhed](https://gitlab.com/jepnel/duelighed/raw/master/sosikkerhed.pdf?inline=false)
- [Slides til havmiljø](https://gitlab.com/jepnel/duelighed/raw/master/havmiljoe.pdf?inline=false)
- [Slides til brand](https://gitlab.com/jepnel/duelighed/raw/master/Brand.pdf?inline=false)



- [Pensum(noget materiale censor tidligere har brugt til undervisning)](https://gitlab.com/jepnel/duelighed/raw/master/duelighedsmateriale.pdf?inline=false)

#### Opgaver:
###### Første kursusgang:   
Opgaver side 113 og 117

###### Anden kursusgang:   
A1-A12 og A14,A17,A20-A22,A24,A26-A32 og B5,B11,B12 

###### Tredje kursusgang:
B17,B21 og B26,B30,B33 og C1,C2,C3 og C14-C19

#### Eksamen:
Duelighedsprøver (Teoretiske del)
Prøven indeholder en Skriftlig faktaprøve og en Sejladsplanlægningsopgave.

Skriftlig Faktaopgave:
30 minutter, der er ingen hjælpemidler tilladt til denne del af prøven.
 
Prøven indeholder spørgsmål om: Bøjer/båker/fyr, søvejsregler (identifikation af skibe, vigeregler, tåge signaler, søsikkerhed og havmiljø. Prøven indeholder ca. 20 spørgsmål.
 
Sejladsplanlægningsopgave:
20 minutter samtale med oplæg fra eksaminanden og spørgsmål fra censor og lærer.
 
En navigationsplanlægningsopgave som eksaminanden har forberedt hjemme og præsenteres ved en mundtlig eksamination. En samtale mellem (lærer,) censor og eksaminanden.
 
Eksaminanden fremlægger sin sejlplan og de overvejelser han/hun har gjort sig og censor (og lærer) spørger ind til konkrete dele eller overvejelser fra eksaminandens fremlæggelse.
 
Bagefter beder censor (og lærer) eksaminanden om uddybende forklaringer på overvejelser, udregninger (st.k.mv., afdrift, deviation eller misvisning), vagthold, alternative planer, vejrændringer, instrukserne i forhold til overholdelse af ruten.
 
Til sidst stiller censor (og lærer) spørgsmål ind til den skriftlige faktaprøve og eventuelle fejl.

Navigationsplanlægningsopgaven modtager du således at du har 2 dages forberedelse.

Det er et stort hold og der er sat to censorer på. Derfor vil vi afholde eksaminationen parralelt i to hold (2 lokaler). Det betyder, at du ikke vil få mulighed for at deltage i alle eksaminationerne. Hvis der er eksminader, der lider af ordblindhed eller andet, kan det have betydning for vedkommende, at du er med ved eksminationen. Lad mig i såfald det vide.

Dagen starter med at alle løser den skriftlige faktaopgave. Censor retter faktaopgaven umiddelbart før eksaminanden skal præsentere sin navigationsplanlægningsopgave, således at faktaopgaven og navigationsplanlægningsopgaven bedømmes som et samlet hele.

#### Eksamens eksempler:
- [Opgave sæt1](https://gitlab.com/jepnel/duelighed/raw/master/faktaopgave-1.doc?inline=false) [Opgave sæt2](https://gitlab.com/jepnel/duelighed/raw/master/faktaopgave-2.doc?inline=false) [Opgave sæt3](https://gitlab.com/jepnel/duelighed/raw/master/faktaopgave-3.pdf?inline=false) [Opgave sæt4](https://gitlab.com/jepnel/duelighed/raw/master/faktaopgave-4.pdf?inline=false) [Opgave sæt5](https://gitlab.com/jepnel/duelighed/raw/master/faktaopgave-5.pdf?inline=false) [Opgave sæt6](https://gitlab.com/jepnel/duelighed/raw/master/faktaopgave-6.pdf?inline=false) [Opgave sæt7](https://gitlab.com/jepnel/duelighed/raw/master/faktaopgave-7.pdf?inline=false) [Opgave sæt8](https://gitlab.com/jepnel/duelighed/raw/master/faktaopgave-8.pdf?inline=false)
- [Navigations eksempler](https://gitlab.com/jepnel/duelighed/raw/master/navigationsplanlaegning-1.docx)

###### Lydbog:
- [Duelighedsbogen som lydbog](http://www.duelighedsbogen.dk/subpages/books/duelighedsbogen/lydbog)